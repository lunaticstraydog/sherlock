/**
 * \file eleusis_client.c
 * \author vincent quentin
 * \date 21 janvier 2021
 * \brief Programme qui gere la partie client du projet
 *
 * \details ce programme permet la création d'un client qui communique avec le serveur
 *
 */
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

pthread_t thread_serveur_tcp_id;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int carte=0;
int theorie =-1 ;
char theorie_str[51454];
int valide=-1;
char gbuffer[256];
char gServerIpAddress[256];
int gServerPort;
char gClientIpAddress[256];
int gClientPort;
char gName[256];
char gNames[4][256];
int gId;
int joueurSel;
int objetSel;
int guiltSel;
int guiltGuess[13];
int tableCartes[4][8];
int b[3];
int goEnabled;
int connectEnabled;
int CestTonTour=0;
char *mess;


volatile int synchro;

/**
 * \fn void void *fn_serveur_tcp(void *arg)
 * \brief Fonction qui se tourne dans un thread séparé et se charge de vérifier qu'il n'y a pas de nouvel envoi de données de la part du serveur, et met une variable a 1 si c'est le cas
 *
 *
 *
 *
 */
void *fn_serveur_tcp()
{
        int sockfd, newsockfd, portno;
        socklen_t clilen;
        struct sockaddr_in serv_addr, cli_addr;
        int n;

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd<0)
        {
                printf("sockfd error\n");
                exit(1);
        }

        bzero((char *) &serv_addr, sizeof(serv_addr));
        portno = gClientPort;
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY;
        serv_addr.sin_port = htons(portno);
       if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        {
                printf("bind error\n");
                exit(1);
        }

        listen(sockfd,5);
        clilen = sizeof(cli_addr);
        while (1)
        {
                newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
                if (newsockfd < 0)
                {
                        printf("accept error\n");
                        exit(1);
                }

                bzero(gbuffer,256);
                n = read(newsockfd,gbuffer,255);
                if (n < 0)
                {
                        printf("read error\n");
                        exit(1);
                }

                pthread_mutex_lock( &mutex );
                synchro=1;
                pthread_mutex_unlock( &mutex );

                while (synchro);
     }
}

/**
 * \fn void sendMessageToServer(char *ipAddress, int portno, char *mess)
 * \brief Fonction pour envoyer un message au serveur
 *
 *
 *
 * \param ipAddress est l ip du serveur
 * \param portno est le port du client
 * \param mess est le message a envoyer
 *
 */
void sendMessageToServer(char *ipAddress, int portno, char *mess)
{
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char sendbuffer[256];

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    server = gethostbyname(ipAddress);
    if (server == NULL)
    {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
    (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
	{
		printf("ERROR connecting\n");
		exit(1);
	}

        sprintf(sendbuffer,"%s\n",mess);
        n = write(sockfd,sendbuffer,strlen(sendbuffer));

	close(sockfd);
}

int main(int argc, char ** argv)
{
	int ret;
	int i,j;
	int popo=5;  // glob
	int unefois=0;
	int boubou=0;

	int c1=0;
	int c2=1;
	int c3=2;
	int c4=3;
	int c5=4;

	int carte_clique=-1;



    int quit = 0;
    SDL_Event event;
	int mx,my;
	char sendBuffer[256];
	char lname[256];
	int id;
	int num_joueur;
	int nb_joueur;
	int deckcarte[52];
	int * deckfinal;
	deckfinal = (int*) malloc (sizeof(int)* 52);

	//int decode =0;

        if (argc<6)
        {
                printf("<app> <Main server ip address> <Main server port> <Client ip address> <Client port> <player name>\n");
                exit(1);
        }

        strcpy(gServerIpAddress,argv[1]);
        gServerPort=atoi(argv[2]);
        strcpy(gClientIpAddress,argv[3]);
        gClientPort=atoi(argv[4]);
        strcpy(gName,argv[5]);

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();

	SDL_Window * window = SDL_CreateWindow("SDL2 eleusis", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 768, 0);

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

	SDL_Surface *deck[52],*connectbutton,*gobutton;


	deck[0] = IMG_Load("0.jpg");
	deck[1] = IMG_Load("1.jpg");
	deck[2] = IMG_Load("2.jpg");
	deck[3] = IMG_Load("3.jpg");
	deck[4] = IMG_Load("4.jpg");
	deck[5] = IMG_Load("5.jpg");
	deck[6] = IMG_Load("6.jpg");
	deck[7] = IMG_Load("7.jpg");
	deck[8] = IMG_Load("8.jpg");
	deck[9] = IMG_Load("9.jpg");
	deck[10] = IMG_Load("10.jpg");

	deck[11] = IMG_Load("11.jpg");
	deck[12] = IMG_Load("12.jpg");
	deck[13] = IMG_Load("13.jpg");
	deck[14] = IMG_Load("14.jpg");
	deck[15] = IMG_Load("15.jpg");
	deck[16] = IMG_Load("16.jpg");
	deck[17] = IMG_Load("17.jpg");
	deck[18] = IMG_Load("18.jpg");
	deck[19] = IMG_Load("19.jpg");
	deck[20] = IMG_Load("20.jpg");


	deck[21] = IMG_Load("21.jpg");
	deck[22] = IMG_Load("22.jpg");
	deck[23] = IMG_Load("23.jpg");
	deck[24] = IMG_Load("24.jpg");
	deck[25] = IMG_Load("25.jpg");
	deck[26] = IMG_Load("26.jpg");
	deck[27] = IMG_Load("27.jpg");
	deck[28] = IMG_Load("28.jpg");
	deck[29] = IMG_Load("29.jpg");
	deck[30] = IMG_Load("30.jpg");


	deck[31] = IMG_Load("31.jpg");
	deck[32] = IMG_Load("32.jpg");
	deck[33] = IMG_Load("33.jpg");
	deck[34] = IMG_Load("34.jpg");
	deck[35] = IMG_Load("35.jpg");
	deck[36] = IMG_Load("36.jpg");
	deck[37] = IMG_Load("37.jpg");
	deck[38] = IMG_Load("38.jpg");
	deck[39] = IMG_Load("39.jpg");
	deck[40] = IMG_Load("40.jpg");

	deck[41] = IMG_Load("41.jpg");
	deck[42] = IMG_Load("42.jpg");
	deck[43] = IMG_Load("43.jpg");
	deck[44] = IMG_Load("44.jpg");
	deck[45] = IMG_Load("45.jpg");
	deck[46] = IMG_Load("46.jpg");
	deck[47] = IMG_Load("47.jpg");
	deck[48] = IMG_Load("48.jpg");
	deck[49] = IMG_Load("49.jpg");
	deck[50] = IMG_Load("50.jpg");
	deck[51] = IMG_Load("51.jpg");

	connectbutton = IMG_Load("connectbutton.png");
	gobutton = IMG_Load("gobutton.png");



	strcpy(gNames[0],"-");
	strcpy(gNames[1],"-");
	strcpy(gNames[2],"-");
	strcpy(gNames[3],"-");

	joueurSel=-1;
	objetSel=-1;
	guiltSel=-1;

	b[0]=-1;
	b[1]=-1;
	b[2]=-1;





	goEnabled=0;
	connectEnabled=1;

	SDL_Texture *texture_deck[52],*texture_connectbutton,*texture_gobutton;

	for (int i=0;i<52;i++)
		texture_deck[i] = SDL_CreateTextureFromSurface(renderer, deck[i]);


	texture_connectbutton = SDL_CreateTextureFromSurface(renderer, connectbutton);
	texture_gobutton = SDL_CreateTextureFromSurface(renderer, gobutton);

	TTF_Font* Sans_75 = TTF_OpenFont("sans.ttf", 75);
	TTF_Font* Sans_20 = TTF_OpenFont("sans.ttf", 20);
	/* Creation du thread serveur tcp. */
	printf ("Creation du thread serveur tcp !\n");
	synchro=0;
	ret = pthread_create ( & thread_serveur_tcp_id, NULL, fn_serveur_tcp, NULL);

	int sockfd, portno, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	char buffer[256];
	char receiveBuffer[256];
	char mess[512];
	char str[512];
	char *mess_tempo;
	char *token;
	int kicekijoue;
	int carteretour=-1;
	while (!quit)
	{
		SDL_SetRenderDrawColor(renderer, 255, 230, 230, 255);
		SDL_Rect rect = {0, 0, 1024, 768};
		SDL_RenderFillRect(renderer, &rect);

		SDL_Rect position;
		SDL_Color color = {0,0,0,255}; //black text
		SDL_Surface *texte = TTF_RenderText_Solid(Sans_75, "Eleusis, le jeu", color);
		position.x = 280;
		position.y = 0;
		position.w = 350;
		position.h = 100;
		SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, texte);
		SDL_RenderCopy(renderer, texture, NULL, &position);
    		SDL_DestroyTexture(texture);
    		SDL_FreeSurface(texte);


		if (argc<4)
		{
			fprintf(stderr,"./client <server_address> <server_numport> <message>\n");
			fprintf(stderr,"ex: ./client localhost 32000 \"joe\"\n");
			return 1;
		}

        if (synchro==1)
        {
                pthread_mutex_lock( &mutex );
		switch (gbuffer[0])
		{
			case 'D':
				strtok ((gbuffer), ";");
				for (int i = 0; i < 52; i++)
				{
					deckcarte[i]= atoi (strtok (NULL, ";"));
				}

				num_joueur = atoi (strtok(NULL, ";"));
				printf("votre numéro est=%d, master\n",num_joueur);

				nb_joueur = atoi (strtok(NULL, ";"));
				printf("Le nombre de joueurs vaut%d\n",nb_joueur);



				// creation deckfinal
				if (nb_joueur==1)
				{
					for (int i = 0; i < 52; i++)
					{
						deckfinal[i]=deckcarte[i];
					}
				}
				else if (nb_joueur==2)
				{


					if (num_joueur==0)
					{
					for (int i = 0; i < 25; i++)
					{
						deckfinal[i]=deckcarte[i];
					}
					}

					if (num_joueur==1)
					{
					for (int i = 0; i < 26; i++)
					{
						deckfinal[i]=deckcarte[26+i];
					}
					}

				}


				else if (nb_joueur==4)
				{


					if (num_joueur==0)
					{
						for (int i = 0; i <12; i++)
						{
							deckfinal[i]=deckcarte[i+0];
						}
					}

					if (num_joueur==1)
					{
						for (int i = 0; i < 13; i++)
						{
							deckfinal[i]=deckcarte[i+13];
						}
					}

					if (num_joueur==2)
					{
						for (int i = 0; i < 13; i++)
						{
							deckfinal[i]=deckcarte[i+26];
						}
					}

					if (num_joueur==3)
					{
						for (int i = 0; i < 13; i++)
						{
							deckfinal[i]=deckcarte[i+39];
						}
					}



				}




				else if (nb_joueur==3)
				{



					if (num_joueur==0)
					{
					for (int i = 0; i < 16; i++)
					{
						deckfinal[i]=deckcarte[i+0];
					}
					}

					if (num_joueur==1)
					{
					for (int i = 0; i < 17; i++)
					{
						deckfinal[i]=deckcarte[i+17];
					}
					}

					if (num_joueur==2)
					{
					for (int i = 0; i < 17; i++)
					{
						deckfinal[i]=deckcarte[i+34];
					}
					}

				}
				carte=1;
						break;
			case 'V': //le serveur dit: La carte jouée est valide et a qui c'est de jouer
				 token=strtok(gbuffer,";"); //On vire le V
				 token=strtok(NULL,";");
				 kicekijoue=atoi(token);
				 if (num_joueur==kicekijoue)
				  {
					  CestTonTour=1;
					  printf("C'est ton tour, rate pas ton coup\n");
				  }

				 else printf("C'est au joueur %d de jouer\n",kicekijoue);
				 carteretour=atoi(strtok(NULL,";"));
				 printf("La carte %d est valide\n",carteretour);
				 valide=1;
				 break;
			case 'I': //le serveur dit: La carte jouée est valide et a qui c'est de jouer
				 token=strtok(gbuffer,";"); //On vire le V
				 token=strtok(NULL,";");
				 kicekijoue=atoi(token);
				 if (num_joueur==kicekijoue)
				  {
					  CestTonTour=1;
					  printf("C'est ton tour, rate pas ton coup\n");
				  }

				 else{
					 printf("C'est au joueur %d de jouer\n",kicekijoue);
					CestTonTour=0;
				 }
				 carteretour=atoi(strtok(NULL,";"));
				 printf("La carte %d est invalide\n",carteretour);
				 valide=0;
				 break;
			case 'J': //le server dit : on commence à jouer et indique le numéro du joueur qui commence
				 token=strtok(gbuffer,";");
				 kicekijoue=atoi(strtok(gbuffer,";"));
				 if (num_joueur==kicekijoue)
				  {
					  CestTonTour=1;
					  printf("On commence le jeu, a toi l'honneur\n");
				  }
				  else {
					  printf("On commence le jeu, c'est au joueur %d de commencer\n",kicekijoue);
					CestTonTour=0;
				  }
				  break;
			case 'G':

				  token=strtok(gbuffer,";");
				  token=strtok(NULL,";");
				  printf("%s\n",token);
				  break	;
			case 'Z':
				  token=strtok(gbuffer,";");
				  token=strtok(NULL,";");
				  printf("%s\n",token);
				  quit=1;
				  break;
			case 'P':
				  token=strtok(gbuffer,";");
				  token=strtok(NULL,";");
				  printf("%s\n",token);
				  break;
			case 'L':
				  token=strtok(gbuffer,";");
				  token=strtok(NULL,";");
				  printf("%s\n",token);
				  break;


		}
		synchro=0;
                pthread_mutex_unlock( &mutex );
	}



	if (SDL_PollEvent(&event))
	{
		//printf("un event\n");
		//printf("%d\n", event.type);


        	switch (event.type)
        	{
            		case SDL_QUIT:
                		quit = 1;
                		break;
			case  SDL_MOUSEBUTTONDOWN:
				SDL_GetMouseState( &mx, &my );
				if ((mx<200) && (my<50) && (connectEnabled==1) )
				{
					sprintf(sendBuffer,"C %s %d %s",gClientIpAddress,gClientPort,gName);
					printf("sendbuffer=%s\n",sendBuffer);

					sendMessageToServer(gServerIpAddress, gServerPort, sendBuffer);

					connectEnabled=0;
					synchro=1;
					printf("on a fini connect=%d\n",1);
				}
				else if ((mx>=290) && (mx<390) && (my>=170) && (my<270) && (CestTonTour==1) )
				{

					printf("entrez la théorie dans le terminal\n");
    					fgets(theorie_str,41454,stdin);
					printf("La théorie entrée est...%s\n",theorie_str);
					strcpy(mess,"T;");
					snprintf( str, 512, "%d", num_joueur );
					strcat(mess,str);
					strcat(mess,";");
					strcat(mess,theorie_str);
					strcat(mess,";");
					printf("mess=%s\n",mess);
					sendMessageToServer(gServerIpAddress, gServerPort, mess);

				}

				else if ((mx>=100) && (mx<250) && (my>=500) && (my<650) && (CestTonTour==1) )
				{
					// on envoi c1 avant de le modif
					carte_clique = c1;
					c1=popo;

        			boubou++;
        			popo++;

				}
				else if ((mx>=260) && (mx<400) && (my>=500) && (my<650)  && (CestTonTour==1))
				{
					carte_clique = c2;
        			c2=popo;

       				boubou++;
       				popo++;
				}
				else if ((mx>=410) && (mx<550) && (my>=500) && (my<650)  && (CestTonTour==1))
				{
					carte_clique = c3;
					c3=popo;

					boubou++;
					popo++;
				}
				else if ((mx>=560) && (mx<700) && (my>=500) && (my<650)  && (CestTonTour==1))
				{
					carte_clique = c4;
					c4=popo;

					boubou++;
					popo++;
				}
				else if ((mx>=710) && (mx<850) && (my>=500) && (my<650)  && (CestTonTour==1))
				{
					carte_clique = c5;
					c5=popo;

					boubou++;
					popo++;

				}
				else if (CestTonTour==0)
				{
					printf("C'est pas ton tour, c'est au joueur %d de jouer\n",num_joueur);
				}



				if (boubou!=0)
				{
					boubou=0;

					if (popo>round(52/(nb_joueur)))
						 {
							 printf("On arrive en bout de packet, on va refaire un tour\n");
							 popo=0;
						 }
					strcpy(mess,"");
					strcat(mess,"C;");
					sprintf(str,"%d", deckfinal[carte_clique]);
					strcat(mess,str);
					strcat(mess,";");


					sendMessageToServer(gServerIpAddress, gServerPort, mess);
					CestTonTour=0;

				}


				break;
        	}
	}






	{  // position/taille des cartes

		if ((carte_clique==-1) && (carte==1)) //faut aussi qu'on aie reçu nos cartes et que l'on aie pas encore reçu celles du serveur
		{

        SDL_Rect dstrect_1 = { 110, 500, 140, 140 };
        SDL_RenderCopy(renderer, texture_deck[deckfinal[0]], NULL, &dstrect_1);
        SDL_Rect dstrect_2 = { 260, 500, 140, 140 };
        SDL_RenderCopy(renderer, texture_deck[deckfinal[1]], NULL, &dstrect_2);
        SDL_Rect dstrect_3 = { 410, 500, 140, 140 };
        SDL_RenderCopy(renderer, texture_deck[deckfinal[2]], NULL, &dstrect_3);
        SDL_Rect dstrect_4 = { 560, 500, 140, 140 };
        SDL_RenderCopy(renderer, texture_deck[deckfinal[3]], NULL, &dstrect_4);
        SDL_Rect dstrect_5 = { 710, 500, 140, 140 };
        SDL_RenderCopy(renderer, texture_deck[deckfinal[4]], NULL, &dstrect_5);
        // Le bouton connect

        unefois++;
    	}

	}


	if (connectEnabled==1)
	{
        	SDL_Rect dstrect = { 0, 0, 200, 50 };
        	SDL_RenderCopy(renderer, texture_connectbutton, NULL, &dstrect);
	}


	if (carte_clique!=-1){
					SDL_Rect dstrect_1 = { 110, 500, 140, 140 };
					SDL_RenderCopy(renderer, texture_deck[deckfinal[c1]], NULL, &dstrect_1);
					SDL_Rect dstrect_2 = { 260, 500, 140, 140 };
					SDL_RenderCopy(renderer, texture_deck[deckfinal[c2]], NULL, &dstrect_2);
					SDL_Rect dstrect_3 = { 410, 500, 140, 140 };
					SDL_RenderCopy(renderer, texture_deck[deckfinal[c3]], NULL, &dstrect_3);
					SDL_Rect dstrect_4 = { 560, 500, 140, 140 };
					SDL_RenderCopy(renderer, texture_deck[deckfinal[c4]], NULL, &dstrect_4);
					SDL_Rect dstrect_5 = { 710, 500, 140, 140 };
					SDL_RenderCopy(renderer, texture_deck[deckfinal[c5]], NULL, &dstrect_5);
        				SDL_Rect dstrect = { 290, 170, 100, 100 };
        				SDL_RenderCopy(renderer, texture_gobutton, NULL, &dstrect);
					SDL_Rect position_th;
					SDL_Color color_th = {0,0,0,255}; //black text
					SDL_Surface *texte_th = TTF_RenderText_Blended(Sans_20, "Ici pour entrer theorie", color_th);
					position_th.x = 250;
					position_th.y = 270;
					position_th.w = 180;
					position_th.h = 35;
					SDL_Texture * texture_th = SDL_CreateTextureFromSurface(renderer, texte_th);
					SDL_RenderCopy(renderer, texture_th, NULL, &position_th);
					SDL_DestroyTexture(texture_th);
					SDL_FreeSurface(texte_th);
	}
					if (carteretour!=-1)
						 {
						SDL_Rect dstrect_retour_i = { 600, 130, 140, 140};
						SDL_RenderCopy(renderer, texture_deck[carteretour], NULL, &dstrect_retour_i);
						 }
					if (valide==1)
						 {
		SDL_Rect position;
		SDL_Color color = {0,0,0,255}; //black text
		SDL_Surface *texte = TTF_RenderText_Blended(Sans_20, "Derniere carte jouee valide", color);
		position.x = 580;
		position.y = 270;
		position.w = 180;
		position.h = 35;
		SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, texte);
		SDL_RenderCopy(renderer, texture, NULL, &position);
    		SDL_DestroyTexture(texture);
    		SDL_FreeSurface(texte);
						 }
					if (valide==0)
						 {
		SDL_Rect position;
		SDL_Color color = {0,0,0,255}; //black text
		SDL_Surface *texte = TTF_RenderText_Blended(Sans_20, "Derniere carte jouee invalide", color);
		position.x = 580;
		position.y = 270;
		position.w = 180;
		position.h = 35;
		SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, texte);
		SDL_RenderCopy(renderer, texture, NULL, &position);
    		SDL_DestroyTexture(texture);
    		SDL_FreeSurface(texte);

						 }
        SDL_RenderPresent(renderer);
    }

    for (int i=0;i<52;i++)
	     {
    		SDL_DestroyTexture(texture_deck[i]);
	     }


    SDL_FreeSurface(deck[0]);
    SDL_FreeSurface(deck[1]);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return 0;
}
