/**
 * \file server.c
 * \author vincent quentin
 * \date 21 janvier 2021
 * \brief Programme qui gere la partie serveur du projet
 *
 * \details ce programme permet la création d'un serveur pour discuter avec un/plusieurs clients
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <ctype.h>
#include <signal.h>


#include <time.h>
#include <netdb.h>
#include <arpa/inet.h>

const char delim1 = '-';
const char delim2[2]=",";
const char spade = 'S';
const char heart = 'H';
const char club = 'C';
const char diamond = 'D';

int *Tab_autorise;

typedef struct Regles {
  int coeur;
  int pique;
  int trefle;
  int carreau;
  char * nombre;
  char * multiple;
  char * suite;
  int coeur_nombre;
  int pique_nombre;
  int trefle_nombre;
  int carreau_nombre;
  int coeur_multiple;
  int pique_multiple;
  int trefle_multiple;
  int carreau_multiple;
}Regle;



struct _client
{
        char ipAddress[40];
        int port;
        char name[40];
} tcpClients[4];
int nbClients;
int fsmServer;
int deck[52];
char theorie_str[51454];

Regle regles;


void error(const char *msg)
{
    perror(msg);
    exit(1);
}










/**
 * \fn int * set_regles_globales (Regle *regles)
 * \brief Fonction pour creer les regles
 *
 *
 * \param Regle est une structure
 *
 *
 */
int * set_regles_globales (Regle *regles)
{
  int *cartes;


  cartes= (int *) malloc (52*sizeof(int));

  for (int i=0;i<52;i++)
     {
       cartes[i]=1;  //1 autorisé, 0 interdit
     }
  if (regles->trefle==1)
     {
       for (int i=1;i<53;i++)
	  {
	    if (i%4==1)
	       {
		 cartes[i-1]=0;
	       }
	  }
     }
  if (regles->carreau==1)
     {
       for (int i=1;i<53;i++)
	  {
	    if (i%4==2)
	       {
		 cartes[i-1]=0;
	       }
	  }
     }
  if (regles->coeur==1)
     {
       for (int i=1;i<53;i++)
	  {
	    if (i%4==3)
	       {
		 cartes[i-1]=0;
	       }
	  }
     }
  if (regles->pique==1)
     {
       for (int i=1;i<53;i++)
	  {
	    if (i%4==0)
	       {
		 cartes[i-1]=0;
	       }
	  }
     }


  //ensuite, les règles plus complexes sur les numéros
  char tab_chaines[50][10]; //tableaux de chaines de char de 10mots de 10 char chacuns
  int test_spe =0 ;

  /* petite explication tout de même: regles->nombre est une chaine de char contenant soit 6-9 ~ 6,7,8,9 ou 1,2,3,4 qui cherche a bannir toutes les cartes séparées par des virgules. Dans la fonction dessous, on sépare en sous chaînes les parties séparées par delim1, et après on gère les sous chaînes de la forme a-b */
  for (int i=0;i<50;i++)
     {
       strcpy(tab_chaines[i],"IIIIIIIIII"); //on initialise les sous chaines a I*10 pour la suite
     }
  int i=0 ;
  char * token;
  char * tokena;
  char * tokenb;



   char chainenulle[15];
   char chainetest[15];
   int ret;


   strcpy(chainenulle, "");
   strcpy(chainetest, regles->nombre);

   ret = strcmp(chainetest, chainenulle);

  token=strtok(regles->nombre, delim2);

  if (token==NULL) //ou il y a un nombre, ou il n'y en a pas
     {
      if (ret==0) //chaine nulle
	 {
	   // il rentrera pas dans le while de toute façon
	 }
      else
  	strcpy(tab_chaines[i],regles->nombre); //c'est le nombre direct

     }

  while (token!=NULL){
  	strcpy(tab_chaines[i],token);
        token=strtok(NULL, delim2);
	i++;
      }
  i=0;

  const char str1='I';
  while (strchr(tab_chaines[i],str1)==NULL)//tant qu'on a des trucs interessant à lire (Twilight ne compte pas )
  {

    token=strchr(tab_chaines[i],delim1);
    if (token!=NULL) //on est dans le cas type 1-5 que l'on remplace par 1,2,3,4,5
       {

      	char *astr=tab_chaines[i];
	char strtesta[10];
	char strtestb[10];
        const char s[2] = "-";
  	strcpy(strtesta,tab_chaines[i]);
        tokena=strtok(strtesta,s);
        tokenb=strtok(NULL,s);
  	int a=atoi(tokena);
	int b=atoi(tokenb);


	test_spe=0;
      for (int j=a;j<b+1;j++)
	{
      if (strchr(tab_chaines[i],spade)!=NULL)
      {
	cartes[(j-1)*4+3]=0;
	test_spe=1;
      }
      if (strchr(tab_chaines[i],heart)!=NULL)
      {
	cartes[(j-1)*4+2]=0;
	test_spe=1;
      }
      if (strchr(tab_chaines[i],diamond)!=NULL)
      {
	cartes[(j-1)*4+1]=0;
	test_spe=1;
      }
      if (strchr(tab_chaines[i],club)!=NULL)
      {
	cartes[(j-1)*4+0]=0;
	test_spe=1;
      }
      if (test_spe==0) //on applique la règle générale
	 {
	   if (regles->coeur_nombre==1)
	      {
		cartes[(j-1)*4+2]=0;
	      }
	   if (regles->pique_nombre==1)
	      {

		cartes[(j-1)*4+3]=0;
	      }
	   if (regles->trefle_nombre==1)
	      {
		cartes[(j-1)*4+0]=0;
	      }
	   if (regles->carreau_nombre==1)
	      {
		cartes[(j-1)*4+1]=0;
	      }
	 }
	}
       }

    else  //bon la c'est juste un chiffre a éviter, donc on le convertit juste et on lévite du fait d'un arrêt soudain des lois de la gravité entrainant l'annulation du DS de MCD
      //on teste aussi voir quelles couleurs
    {

      test_spe=0;
      if (strchr(tab_chaines[i],spade)!=NULL)
      {
	cartes[(atoi(tab_chaines[i])-1)*4+3]=0;
	test_spe=1;
      }
      if (strchr(tab_chaines[i],heart)!=NULL)
      {
	cartes[(atoi(tab_chaines[i])-1)*4+2]=0;
	test_spe=1;
      }
      if (strchr(tab_chaines[i],diamond)!=NULL)
      {
	cartes[(atoi(tab_chaines[i])-1)*4+1]=0;
	test_spe=1;
      }
      if (strchr(tab_chaines[i],club)!=NULL)
      {
	cartes[(atoi(tab_chaines[i])-1)*4+0]=0;
	test_spe=1;
      }
      if (test_spe==0) //on applique la règle générale
	 {
	   if (regles->coeur_nombre==1)
	      {
		cartes[(atoi(tab_chaines[i])-1)*4+2]=0;
	      }
	   if (regles->pique_nombre==1)
	      {
		cartes[(atoi(tab_chaines[i])-1)*4+3]=0;
	      }
	   if (regles->trefle_nombre==1)
	      {
		cartes[(atoi(tab_chaines[i])-1)*4+0]=0;
	      }
	   if (regles->carreau_nombre==1)
	      {
		cartes[(atoi(tab_chaines[i])-1)*4+1]=0;
	      }
	 }

    }
    i++;
  }
  //maintenant on passe aux multiples
  for (int i=0;i<50;i++)
     {
       strcpy(tab_chaines[i],"IIIIIIIIII"); //on initialise les sous chaines a I*10 pour la suite
     }

  i =0 ;
  token=strtok(regles->multiple, delim2);



   strcpy(chainenulle, "");
   strcpy(chainetest, regles->multiple);

   ret = strcmp(chainetest, chainenulle);

  if (token==NULL) //ou il y a un nombre, ou il n'y en a pas
     {
      if (ret==0) //chaine nulle
	 {
	   // il rentrera pas dans le while de toute façon
	 }
      else
  	strcpy(tab_chaines[i],regles->multiple); //c'est le nombre direct

     }





  while (token!=NULL){
  	strcpy(tab_chaines[i],token);

        token=strtok(NULL, delim2);
	i++;
      }
  i=0;

  while (strchr(tab_chaines[i],str1)==NULL)//tant qu'on a des trucs interessant à lire (Twilight ne compte pas )
  {

    token=strchr(tab_chaines[i],delim1);
    if (token!=NULL) //on est dans le cas type 1-5 que l'on remplace par 1,2,3,4,5
       {
      	char *astr=tab_chaines[i];
	char strtesta[10];
	char strtestb[10];
        const char s[2] = "-";
  	strcpy(strtesta,tab_chaines[i]);
        tokena=strtok(strtesta,s);
        tokenb=strtok(NULL,s);
  	int a=atoi(tokena);
	int b=atoi(tokenb);

	test_spe=0;
	for (int j=a;j<b+1;j++)
	{
	int c=j;
	int ctemp =c ;
	while (c<=13)
	{

      if (strchr(astr,spade)!=NULL)
      {
	cartes[(c-1)*4+3]=0;
	test_spe=1;
      }
      if (strchr(astr,heart)!=NULL)
      {
	cartes[(c-1)*4+2]=0;
	test_spe=1;
      }
      if (strchr(astr,diamond)!=NULL)
      {
	cartes[(c-1)*4+1]=0;
	test_spe=1;
      }
      if (strchr(astr,club)!=NULL)
      {
	cartes[(c-1)*4+0]=0;
	test_spe=1;
      }
      if (test_spe==0) //on applique la règle générale
	 {
	   if (regles->coeur_multiple==1)
	      {
		cartes[(c-1)*4+2]=0;
	      }
	   if (regles->pique_multiple==1)
	      {
		cartes[(c-1)*4+3]=0;
	      }
	   if (regles->trefle_multiple==1)
	      {
		cartes[(c-1)*4+0]=0;
	      }
	   if (regles->carreau_multiple==1)
	      {
		cartes[(c-1)*4+1]=0;
	      }
	 }



        c+=ctemp;
	}
	}

       }
    else
    {
      	char *astr=tab_chaines[i];
	int c=atoi(tab_chaines[i]);
	int ctemp =c ;
	while (c<=13)
	{
      test_spe=0;
      if (strchr(astr,spade)!=NULL)
      {
	cartes[(c-1)*4+3]=0;
	test_spe=1;
      }
      if (strchr(astr,heart)!=NULL)
      {
	cartes[(c-1)*4+2]=0;
	test_spe=1;
      }
      if (strchr(astr,diamond)!=NULL)
      {
	cartes[(c-1)*4+1]=0;
	test_spe=1;
      }
      if (strchr(astr,club)!=NULL)
      {
	cartes[(c-1)*4+0]=0;
	test_spe=1;
      }
      if (test_spe==0) //on applique la règle générale
	 {
	   if (regles->coeur_multiple==1)
	      {
		cartes[(c-1)*4+2]=0;
	      }
	   if (regles->pique_multiple==1)
	      {
		cartes[(c-1)*4+3]=0;
	      }
	   if (regles->trefle_multiple==1)
	      {
		cartes[(c-1)*4+0]=0;
	      }
	   if (regles->carreau_multiple==1)
	      {
		cartes[(c-1)*4+1]=0;
	      }
	 }



        c+=ctemp;
	}
    }

    i++;
  }


  return cartes;
}




/**
 * \fn void printClients()
 * \brief Fonction pour afficher les informations d un client
 *
 *
 *
 *
 */
void printClients()
{
        int i;

        for (i=0;i<nbClients;i++)
                printf("%d: %s %5.5d %s\n",i,tcpClients[i].ipAddress,
                        tcpClients[i].port,
                        tcpClients[i].name);
}

/**
 * \fn int findClientByName(char *name)
 * \brief Fonction pour retrouver un client
 *
 *
 * \param name est le nom d un client
 *
 */
int findClientByName(char *name)
{
        int i;

        for (i=0;i<nbClients;i++)
                if (strcmp(tcpClients[i].name,name)==0)
                        return i;
        return -1;
}

 /**
 * \fn void sendMessageToClient(char *clientip,int clientport,char *mess)
 * \brief Fonction pour envoyer un message au client
 *
 *
 *
 * \param clientip est l ip du client
 * \param clientport est le port du client
 * \param mess est le message a envoyer
 *
 */
void sendMessageToClient(char *clientip,int clientport,char *mess)
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[256];

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    server = gethostbyname(clientip);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(clientport);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        {
                printf("ERROR connecting\n");
                exit(1);
        }

        sprintf(buffer,"%s\n",mess);
        n = write(sockfd,buffer,strlen(buffer));

    close(sockfd);
}

 /**
 * \fn void broadcastMessage(char *mess)
 * \brief Fonction pour envoyer un message a tout les clients
 *
 *
 *
 *
 * \param mess est le message a envoyer
 *
 */
void broadcastMessage(char *mess)
{
        int i;

        for (i=0;i<nbClients;i++)
                sendMessageToClient(tcpClients[i].ipAddress, tcpClients[i].port, mess);
}

int main(int argc, char *argv[])
{
     char nb_joueur[256];
     printf("Combien de joueurs désirez vous ,maître? (Entre 1 et 4 joueurs)\n");
     scanf("%s",nb_joueur);
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     char receiveBuffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
	   int i;
     Regle regles;

     //////////////////////////////////////////////////////////////

// creation du deck de carte du joueurs


  srand(time(NULL));

    int nbr_cartes=52;
    int deckcard[52];

    //initialisation a -1 pour éviter le segfault


          for (int i=0;i<nbr_cartes;i++)
      {
        deckcard[i]=-1;
      }


    i=0;
    int alea=(rand()%52);
    int cpt=0;

    while (i <nbr_cartes){



          for (int l=0;l<nbr_cartes;l++)
      {
        if (deckcard[l]==alea)
            {
        cpt=1;
            }
      }

    if (cpt==0)
        {

          deckcard[i]=alea;

          i++;

        }
    alea=(rand()%52);

    cpt=0;

    }





    regles.coeur=0;
    regles.pique=0;
    regles.trefle=0;
    regles.carreau=0;
    regles.nombre=" ";
    regles.multiple="99X";
/* mettre a 0 pour autoriser les coeurs dans regles.nombre même si ce n'est pas indiqué
   Par exemple si regles.coeur_multiple=0, et que multiples= 2X, alors les multiples de 2 coeurs sont autorisés quand même*/
    regles.coeur_nombre=1;
    regles.pique_nombre=1;
    regles.trefle_nombre=1;
    regles.carreau_nombre=1;
    regles.coeur_multiple=1;
    regles.pique_multiple=1;
    regles.trefle_multiple=1;
    regles.carreau_multiple=1;


    int tempo=2;

    printf("regle qui interdit les trèfles ? 1 oui 0 non \n");
    scanf("%d",&tempo);
    if (tempo ==1)
    {
        regles.trefle=1;
    }

    printf("regle qui interdit les piques ? 1 oui 0 non \n");
    scanf("%d",&tempo);
    if (tempo ==1)
    {
        regles.pique=1;
    }

    printf("regle qui interdit les coeur ? 1 oui 0 non \n");
    scanf("%d",&tempo);
    if (tempo ==1)
    {
        regles.coeur=1;
    }

    printf("regle qui interdit les carreau ? 1 oui 0 non \n");
    scanf("%d",&tempo);
    if (tempo ==1)
    {
        regles.carreau=1;
    }

    char test[50];
    printf("Entrez ici les nombres a interdire: Séparés par une , ou un -\n");
    printf("par exemple entrez 1,2-6 pour interdire 1,2 3 4 5 6) \n");
    printf("plus compliqué: entrez 1S-3S pour interdire les 1 a 3 de pique\n");
    printf("Vous pouvez procéder de la même façon avec H=coeurs,C=trèfles,D=carreau\n");
    printf("dernier exemple: 1H-3H,5S,6 enleve les 1a 3 de coeur,le 5 de pique,et tous les 6 \n");
    printf("entrez -1 pour ne pas mettre de règle\n");
    scanf("%s",test);
    if (atoi(test)==-1)
	     {
		    regles.nombre=" " ;
	     }
    else regles.nombre=test;

    char testyty[50];
    printf("Entrez ici les multiples a interdire:Séparés par une , ou un -\n");
    printf("par exemple entrez 2X-6X pour interdire les multiples de 2 a 6) \n");
    printf("plus compliqué: entrez 3XS-5XS pour interdire les multiples de 3 a 5 de pique\n");
    printf("Vous pouvez procéder de la même façon avec H=coeurs,C=trèfles,D=carreau\n");
    printf("dernier exemple: 3XH-5XH,5XS,6X enleve les de 3 a 5 de coeur,les multiples de  5 de pique,et tous les multiples de 6 \n");
    printf("entrez -1 pour ne pas mettre de règle\n");

    scanf("%s",testyty);
    if (atoi(testyty)==-1)
	     {
		    regles.multiple="99X" ; //parce que il n'accepte pas une règle vide
	     }
    else regles.multiple=testyty;

    printf("On peut aussi modifier les valeurs de règles.coeur_nombre (a zero) pour indiquer que tous les nombres indiqués dans règles.nombre de couleur coeur sont interdits, les autres restent alors intacts. Ca marche mais c'est compliqué sans UI, du coup modifiez ça dans le code (en plus c'est pas super utile)\n");
   printf("Par exemple si regles.coeur_multiple=0, et que multiples= 2X, alors les multiples de 2 coeurs sont autorisés quand même\n");






        char com;
        char clientIpAddress[256], clientName[256];
        int clientPort;
        int id;
        char reply[256];


     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0)
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0)
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);




	Tab_autorise = set_regles_globales(&regles);  //on crée un tableau des cartes





	for (i=0;i<atoi(nb_joueur);i++)
	{
        	strcpy(tcpClients[i].ipAddress,"localhost");
        	tcpClients[i].port=-1;
        	strcpy(tcpClients[i].name,"-");
	}

     int num_joueur =0 ;
  	  char * token;
          char mess[10000];

          char str[10000];

          char mess_tempo[10000];
	  printf("On attend les clients tranquillou bilou\n");
     while (1)
     {
     	newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
     	if (newsockfd < 0) error("ERROR on accept");

     	bzero(buffer,256);
     	n = read(newsockfd,buffer,255);
     	if (n < 0) error("ERROR reading from socket");


        if (fsmServer==0)
        {
		strcpy(mess,"");
        	switch (buffer[0])
        	{
                	case 'C':
                        	sscanf(buffer,"%c %s %d %s", &com, clientIpAddress, &clientPort, clientName);

                        	// fsmServer==0 alors j'attends les connexions de tous les joueurs
                                strcpy(tcpClients[nbClients].ipAddress,clientIpAddress);
                                tcpClients[nbClients].port=clientPort;
                                strcpy(tcpClients[nbClients].name,clientName);
                                nbClients++;

                                //printClients();

				// rechercher l'id du joueur qui vient de se connecter

                                id=findClientByName(clientName);
                                printf("id=%d\n",id);

				// lui envoyer un message personnel pour lui communiquer son id

                                sprintf(reply,"I %d",id);
                                //sendMessageToClient(tcpClients[id].ipAddress, tcpClients[id].port, reply);

				// Envoyer un message broadcast pour communiquer a tout le monde la liste des joueurs actuellement
				// connectes

                                sprintf(reply,"L %s %s %s %s", tcpClients[0].name, tcpClients[1].name, tcpClients[2].name, tcpClients[3].name);
                                //broadcastMessage(reply);

				// Si le nombre de joueurs atteint 4, alors on peut lancer le jeu

                                if (nbClients==atoi(nb_joueur))
				{
					// On envoie ses cartes au joueur 0, ainsi que la ligne qui lui correspond dans tableCartes
				    strcat(str, "D;");
				    strcat(mess,str);
				    for (int i = 0; i < 52; i++)
					  {
					    sprintf(str,"%d", deckcard[i]);
					    strcat(str, ";");
					    strcat(mess,str);
					    //printf("%s\n", mess);
					  }


				  for (int i = 0; i < atoi(nb_joueur); i++) // mettre nb joueur
				  {
				    strcpy (mess_tempo,mess);
				    sprintf(str,"%d", i);
				    strcat(str, ";");
				    strcat(mess_tempo, str);
				    strcat(mess_tempo, nb_joueur);
				    strcat(mess_tempo, ";");
				    sendMessageToClient (tcpClients[i].ipAddress, tcpClients[i].port, mess_tempo);
				  }
				  fsmServer=1;
				  strcpy(mess,"J;");
				  sprintf(str,"%d",0);
				  strcat(mess,str);
				  broadcastMessage(mess);
				  printf("On a envoyé toutes les cartes, les joueurs commencent à jouer...\n");


				}
				  break;
			}
	}
	else if (fsmServer==1)
	{
		strcpy(mess,"");
		switch (buffer[0])
		{
                	case 'C':
				  token=strtok(buffer, ";");
				  token=strtok(NULL, ";");
				  num_joueur++;
				if (num_joueur>=atoi(nb_joueur))
		   			{
			   			num_joueur=0;
		   			}

				  if (Tab_autorise[atoi(token)]) //La carte reçue est autorisée
				   {

					  strcat(mess,"V;"); //V for valide
					  sprintf(str,"%d;%d",num_joueur,atoi(token));
					  strcat(mess,str);
					  strcat(mess,";");
					  broadcastMessage(mess);
				   }
				  else
				  {
					  strcat(mess,"I;"); //I for invalide
					  sprintf(str,"%d;%d",num_joueur,atoi(token));
					  strcat(mess,str);
					  strcat(mess,";");
					  broadcastMessage(mess);

				  }
				break;

			case 'T':
				token=strtok(buffer,";");
				token=strtok(NULL,";");
				int nj=atoi(token);
				token=strtok(NULL,";");
				printf("La théorie suivante vous est proposée, est elle bonne (oui/non)?:\n%s\n",token);
				char theorie[100];
				strcpy(theorie,token);
				strcpy(token,"");
				scanf("%s",theorie_str);
				printf("theorie_str=%s\n",theorie_str);
				if (theorie_str[0]=='o')
					 {
						strcpy(mess,"G;Vous avez gagné monsieur. Un point bonus SVP?;");
						sendMessageToClient (tcpClients[nj].ipAddress,tcpClients[nj].port, mess);
					  	sprintf(str,"Z; le joueur %s vient de gagner la partie;",tcpClients[nj].name);
						printf("str=%s\n",str);
						broadcastMessage(str);
					 }
				else
				{
						strcpy(mess,"P;Vous avez perdu spece de nul. Un point bonus SVP?;");
						sendMessageToClient (tcpClients[nj].ipAddress,tcpClients[nj].port, mess);
					  	sprintf(str,"L; le joueur %s vient proposer la théorie suivante:%s mais comme il est nul c'est faux. Faites ouuuuh les autres.;",tcpClients[nj].name,theorie);
						broadcastMessage(str);

				}
				break;


                	default:
                        	break;
			}
	}

     	close(newsockfd);
        } //FIN DU WHILE
     close(sockfd);
     return 0;
}
