var indexSectionsWithContent =
{
  0: "_bcdefghijmnoprstv",
  1: "_r",
  2: "ers",
  3: "befmps",
  4: "bcdfghijmnoprstv",
  5: "r",
  6: "e"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Pages"
};

