var struct_regles =
[
    [ "carreau", "struct_regles.html#a7f680c33ce38999fb3cc6a0354d6fbbe", null ],
    [ "carreau_multiple", "struct_regles.html#a78175c0d70ef4c8ed4b4102c98b1c9af", null ],
    [ "carreau_nombre", "struct_regles.html#aadd5a3450d094d7d9b4a1cce079657fd", null ],
    [ "coeur", "struct_regles.html#a491415d1f13cc56050f05c119f1bd094", null ],
    [ "coeur_multiple", "struct_regles.html#ab316755a6f667026e23bc3dacf0c5695", null ],
    [ "coeur_nombre", "struct_regles.html#a33cee69b02f77f321f85bf37806666f2", null ],
    [ "multiple", "struct_regles.html#afe728cd2483f1a66e1221cab60aa586c", null ],
    [ "nombre", "struct_regles.html#a2a4ee52b46cf8d757bd19a4871dafb73", null ],
    [ "pique", "struct_regles.html#a28e80547cc92e87b105e5ec690f0c90a", null ],
    [ "pique_multiple", "struct_regles.html#ad005ea7cc683b7744ab4c1df20d397a2", null ],
    [ "pique_nombre", "struct_regles.html#a11db2a4501d89e8540aa9e6836895263", null ],
    [ "suite", "struct_regles.html#a903ab2b353b661ccb07061b123fc9988", null ],
    [ "trefle", "struct_regles.html#aebe6b3dfb70d7637226203d0b4b858cf", null ],
    [ "trefle_multiple", "struct_regles.html#a075a29d7abb7012aedfcc7e754ebd1b9", null ],
    [ "trefle_nombre", "struct_regles.html#a84ed4643243372d7cb120e92f717dafc", null ]
];