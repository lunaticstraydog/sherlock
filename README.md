# Eleusis reloaded

# Une documentation plus complète est disponible dans documentation/A_DOCUMENTATION.html (produite par doxygen)

Projet Eleusis, Rob4, Quentin Levent et Vincent Morin    21/01/2021

Présentation du jeu :

Le jeu se joue de 1 à 4 joueurs, un maitre du jeu et un jeux de 52 cartes. Un joueur (appelé « maitre du jeu ») détermine une règle secrète (appelée « règle du monde ») qui définit quelles cartes auront le droit d'être posées.
Les autres joueurs doivent essayer de la deviner en faisant des expériences, c'est-à-dire en proposant à tour de rôle des cartes qui seront soit acceptées soit rejetées par le maitre du jeu en suivant sa règle du monde.
Les propositions de cartes sont connues de tout les joueurs, c'est à dire que si un joueur propose une carte, tout les joueurs verront si elle est acceptée ou non.

Présentation de notre programme :

Le maitre du jeu gère le serveur. Les joueurs sont les clients.
On demande au maitre du jeu (terminal du serveur) de choisir, le nombre de joueurs, ainsi que la ou les règles qu il souhaite appliquer.
Ensuite, les joueurs se connecte et la partie commence quand le nombre de joueurs choisit par le maitre du jeu est atteind.
Lorsqu un joueur pense avoir trouvé la regle, il l ecrit et l envoi au maitre du jeu qui la valide ou non.
La partie se termine quand un joueur a trouvé la regle.




Exemple de lancement du programme : (à faire dans l ordre suivant)


-Ouvrir un terminal (serveur) et compiler le programme :
gcc -o server -g server.c

-Lancer le serveur :
./server <port>
par exemple:
./server 32000

-Rentrer le nombre de joueurs :
entre 1 et 4
Nous conseillons de jouer à 1 ou 2 joueurs,surtout sur une seule machine car ça demande beaucoup de ressources...

-Rentrer les règles :
Saisie dans le terminal du serveur ce qui est si gentiment demandé



-Ouvrir un terminal (client) et compiler le programme :
gcc -o eleusis_client -I/usr/include/SDL2 -g eleusis_client.c -lSDL2_image -lSDL2_ttf -lSDL2 -lpthread

-Lancer le/les clients sur autant de terminaux qu il y a des clients et executer le programme sur chacun des terminaux:
./eleusis_client localhost <port_distant> <adresse_ip_server>(localhost par ex) <port local> <addresse_ip_client> <pseudo>
par exemple
./eleusis_client localhost 32000 localhost 32001 Jabba


-Appuyer sur le bouton 'connect' sur toute les fenetres qui se sont ouverte. (à faire autant de fois qu il y aura de fenetre/client)

Lorsque tout les clients sont connectés, on distribut les cartes et le jeu commence.
Pour jouer une carte, il faut cliquer dessus, un message s'affiche si la carte peut etre jouée.

-Sur chaque fenetre on voit la carte jouée par les autres joueurs en haut à droite

- IL FAUT SURVEILLER LE TERMINAL !!
-Que ce soit au niveau du client au du serveur, beaucoup d'infos apparraissent au niveau du terminaL.
-Chaque joueur peut jouer lorsque c est son tour, un message apparait si il clique sur une carte quand ce n est pas son tour de jouer

-Appuyer sur le bouton 'go button' pour soumettre une regle :
Un message apparait dans le terminal. Ecrire une regle et appuyer sur entrer

-Une fois la regle proposé, il faut la valider ou non, dans le terminal du serveur.
Ecrire oui ou non dans le terminal du serveur selon si la regle est bonne ou mauvaise


-Si la regle est bonne et validé par le maitre du jeu, le jeu s arrete en donnant le vainqueur.

-Sinon la partie continue jusqu a ce qu un autre joueur propose une regle.




Sur un plan plus personnel, nous avons eu beaucoup de mal avec ce projet.
Nous ne pensions pas prendre autant de temps pour terminer ce projet. (environ 45h en tout, car nous avons du refaire beaucoup de chose)
Nous avons eu beaucoup de probleme pour regler notamment le fait de partager les prises de decisions d un joueurs avec les autres. (passer par le serveur qui renvoit la décision à tous sans modifier les leurs...)
Nous avons donc privilégié le code et les fonctionnalitées plutot que la partie graphisme qui est assez sommaire.
La bibliothèque graphique n'étant pas adapté pour des boutons radios et autres, nous avons donc choisi de faire ça en ligne de commande ce qui est un choix respectable.

De plus gérer les différents nombres de joueurs était relativement chronophage (nous avons du faire beaucoup de cas en fonction des différents nombres de joueurs)

Nous n'avions jamais fait de diagramme UML, mais nous esperons avoir répondu à cette problématique du mieux possible.
Nous avons aussi fait une documentation pour les fonctions.



xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
Enfin, concernant les regles, nous vous prions de suivre les instructions/exemples dans le terminal.
En effet, les regles peuvent etre très poussée (on peut enlever une seule carte, un groupe de carte (multiple, multiple en prennant en compte les couleurs...)).
Ainsi, il est faut nécessairement entrer des regles à chaque fois qu elles sont demandées au maitre du jeu.
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
